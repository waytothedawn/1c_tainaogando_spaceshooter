﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

      public void TitleScreen()
    {
        SceneManager.LoadScene("TitleScreen");
    }
    // Start is called before the first frame update
    public void GameScreen()
    {
        SceneManager.LoadScene("GameScreen");
    }

    public void GameOverScene()
    {
        SceneManager.LoadScene("GameOverScene");
    }
  
    // Update is called once per frame
    public void Exit()
    {
        Application.Quit();
    }
}
