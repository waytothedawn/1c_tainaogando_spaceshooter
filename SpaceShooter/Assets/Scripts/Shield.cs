﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    // Start is called before the first frame update
  [SerializeField] BoxCollider2D collider;
  [SerializeField] GameObject graphics;
    Vector2 speed;
    public AudioSource audio;

    void Awake()
    {
        speed.x = Random.Range(-1,1);
        speed.y = Random.Range(0,0); 
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
    }
        public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Player"){
        
            StartCoroutine(DestroyShield());
        }else if(other.tag == "Finish") {
        Destroy(this.gameObject);
        }

    }

    IEnumerator DestroyShield(){

        //Desactivo el grafico
        graphics.gameObject.SetActive(false);
        //Elimino el BoxCollider2D
        collider.enabled = false;
        //Lanzo sonido de explosion
        audio.Play();
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

}
