﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Enemy2Behaviour : MonoBehaviour
{
    [SerializeField] GameObject[] sprites;
    private int elegido;
 
    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    private float timeCounter;
    private float timeToShoot;
    private ScoreManager sm;
    private float timeShooting;
    private float speedx;

    public float timeTorcid;
    private float currentTimeTorcid = 0;
    private bool isShooting;
 
    [SerializeField] GameObject bullet;
 
    private void Awake() {
       
        elegido = Random.Range(0,sprites.Length);
 
        for(int e=0;e<sprites.Length;e++){
            sprites[e].SetActive(false);
        }
 
        sprites[elegido].SetActive(true);

        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
        Inicitialization();
    }
 
    protected virtual void Inicitialization(){
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        speedx = 3.0f;
        isShooting = false;
    }
 
    protected virtual void EnemyBehave(){
        timeCounter += Time.deltaTime;
        currentTimeTorcid += Time.deltaTime;

        if(timeCounter>timeToShoot){
            if(!isShooting){
                isShooting = true;
                Instantiate(bullet,this.transform.position,Quaternion.Euler(0,0,180),null);
            }
            if(timeCounter>(timeToShoot+timeShooting)){
                timeCounter = 0.0f;
                isShooting = false;
            }
        } if(currentTimeTorcid<timeTorcid){
            transform.Translate (-speedx * Time.deltaTime,0, 0);
        }else if(currentTimeTorcid>timeTorcid && currentTimeTorcid<timeTorcid*2){
            transform.Translate (0, -speedx * Time.deltaTime,0);        
        }else if(currentTimeTorcid>timeTorcid*2 && currentTimeTorcid<timeTorcid*3){
            transform.Translate (-speedx * Time.deltaTime,0, 0);
        }else if(currentTimeTorcid>timeTorcid*3 && currentTimeTorcid<timeTorcid*5){
            transform.Translate (0, speedx * Time.deltaTime,0);
        }else if(currentTimeTorcid>timeTorcid*5 && currentTimeTorcid<timeTorcid*6){
            transform.Translate (-speedx * Time.deltaTime,0, 0);
        }else if(currentTimeTorcid>timeTorcid*6 && currentTimeTorcid<timeTorcid*7){
            transform.Translate (0, -speedx * Time.deltaTime,0);
        }else{
            currentTimeTorcid = 0;
        }

    }
 
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        EnemyBehave();
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Laser") {
            StartCoroutine(DestroyShip());
            sm.AddScore(100);
        } else if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }
    }
 
    IEnumerator DestroyShip(){
        //sumar puntos

        //Desactivo el grafico
        sprites[elegido].SetActive(false);
 
        //Elimino el BoxCollider2D
        collider.enabled = false;
 
        //Lanzo la partícula
        ps.Play();
 
        //Lanzo sonido de explosion
        audioSource.Play();
 
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
     
        Destroy(this.gameObject);
    }
}