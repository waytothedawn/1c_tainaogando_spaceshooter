﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManager : MonoBehaviour
{
    public GameObject ShieldPrefab;
    public float timeLaunchShield = 60;

    private float currentTime = 0;

    void Awake(){

        }
    
        void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchShield){
            currentTime = 0;
            StartCoroutine(ThrowShield());
        }
    }

    IEnumerator ThrowShield(){
        while(true){
            Instantiate(ShieldPrefab,new Vector3(10,Random.Range(-1,1)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLaunchShield);
        }
    }

}