﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    public GameObject AsteroidPrefab;
    public float timeLauchAsteroid;

    private float currentTime = 0;

    void Awake(){
        StartCoroutine(ThrowAsteroids());
    }


    IEnumerator ThrowAsteroids(){
        while(true){
            Instantiate(AsteroidPrefab,new Vector3(10,Random.Range(-4,4)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchAsteroid);
        }
    }

}
