﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BossBehaviour : MonoBehaviour
{
    [SerializeField] GameObject[] sprites;
    private int elegido;
 
    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource bossmusic;
    [SerializeField] GameObject bullet;

    private float timeCounter;
    private float timeToShoot;
    private float timeShooting;

    private float speedx;
    private bool isShooting;

    private GameObject enemymanager;
    private GameObject music;
    private GameObject enemy2manager;

    private ScoreManager sm;

    private void Awake() {
        enemymanager = GameObject.Find("EnemyManager");
        music = GameObject.Find("SoundManager");
        enemy2manager = GameObject.Find("Enemy2Manager");
        elegido = Random.Range(0,sprites.Length);
 
        for(int e=0;e<sprites.Length;e++){
            sprites[e].SetActive(false);
        }
 
        sprites[elegido].SetActive(true);
        music.SetActive(false);
        bossmusic.Play();
        Inicitialization();
    }
 
    protected virtual void Inicitialization(){
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        speedx = 3.0f;
        isShooting = false;
    }
 
    protected virtual void EnemyBehave(){

        enemymanager.SetActive(false);
        enemy2manager.SetActive(false);
        timeCounter += Time.deltaTime;
 
        if(timeCounter>timeToShoot)
        {
            if(!isShooting)
            {
                isShooting = true;
                Instantiate(bullet,new Vector3(4,3,0),Quaternion.Euler(0,0,180),null);
                Instantiate(bullet,new Vector3(4,-3,0),Quaternion.Euler(0,0,180),null);
            }
            if(timeCounter>(timeToShoot+timeShooting))
            {
                timeCounter = 0.0f;
                isShooting = false;
            }
            if (transform.position.x < 6)
            {
                transform.position = new Vector3(6,0,0);
            }
            else 
            {
                transform.Translate(-speedx*Time.deltaTime,0,0);
            }
        }        
    }

    void Update()
    {
        EnemyBehave();
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Laser") {
           StartCoroutine(Mortal());
        }
    }

IEnumerator Mortal(){ 
       
        audioSource.Play();
        ps.Play();

        sprites[elegido].SetActive(true);
        for(int i=0; i<15;i++){
        sprites[elegido].SetActive(false);
        yield return new WaitForSeconds(0.1f);
        sprites[elegido].SetActive(true);
        yield return new WaitForSeconds(0.1f);
        sprites[elegido].SetActive(false);
        }
        Destroy(this.gameObject);
        music.SetActive(true);
        enemymanager.SetActive(true);
        }
}