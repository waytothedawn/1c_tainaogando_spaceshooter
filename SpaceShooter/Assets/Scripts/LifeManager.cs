﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour
{
    public GameObject LifePrefab;
    public float timeLauchLife;

    private float currentTime = 0;

    void Awake(){
        StartCoroutine(ThrowLives());
    }


    IEnumerator ThrowLives(){
        while(true){
            Instantiate(LifePrefab,new Vector3(10,Random.Range(-3,3)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchLife);
        }
    }

}
