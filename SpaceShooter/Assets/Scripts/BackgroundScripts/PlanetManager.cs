﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{
    public GameObject PlanetPrefab;
    public float timeLauchPlanet = 10;

    private float currentTime = 0;

    void Awake(){

    }

    // Update is called once per frame
    
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchPlanet){
            currentTime = 0;
           StartCoroutine(ThrowPlanets());
        }
    }
    

    IEnumerator ThrowPlanets(){
        while(true){
            Instantiate(PlanetPrefab,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchPlanet);
        }
    }

}