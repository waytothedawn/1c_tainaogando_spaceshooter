﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonManager : MonoBehaviour
{
    public GameObject MoonPrefab;
    public float timeLauchMoon;

    private float currentTime = 1;

    void Awake(){
        StartCoroutine(ThrowMoons());
    }

    IEnumerator ThrowMoons(){
        while(true){
            Instantiate(MoonPrefab,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchMoon);
        }
    }

}