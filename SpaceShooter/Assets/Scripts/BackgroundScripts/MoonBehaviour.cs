﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    Vector2 speed;


    Transform graphics;
    void Awake()
    {
        speed.x = Random.Range(-2,-1);
        speed.y = Random.Range(0,0); 
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
    }
        public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }   
    }
}
