﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public float timeLauchEnemy;

    private float currentTime = 5;

    void Awake(){

    }

    // Update is called once per frame
    
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchEnemy){
            currentTime = 0;
            StartCoroutine(ThrowEnemy());
        }
    }
    
    IEnumerator ThrowEnemy(){
        while(true){
            Instantiate(EnemyPrefab,new Vector3(10,Random.Range(-4,4)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchEnemy);
        }
    }

}