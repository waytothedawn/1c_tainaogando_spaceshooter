﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class LaserWeapon : Weapon
{
 
    public GameObject LaserBullet;
    public float cadencia;
 
    public override float GetCadencia()
    {
        return cadencia;
    }
 
    public override void Shoot()
    {
        Instantiate (LaserBullet, this.transform.position, Quaternion.identity, null);

    }
}