﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class ScoreManager : MonoBehaviour
{
    [SerializeField] Text lives;
    [SerializeField] Text score;
    [SerializeField] Text gems;

    private int scoreInt;
    private int livesCount;
    private int gemCount;
 
    // Start is called before the first frame update
    void Start()
    {
        scoreInt = 0;
        score.text = scoreInt.ToString("000000");
        livesCount = 3;
        lives.text = livesCount.ToString("0");
        gemCount = 0;
        gems.text = gemCount.ToString("0");
    }
 
    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
    }

    public void QuitLives(int value){
        livesCount -=value;
        lives.text = livesCount.ToString("0");
    }

    public void GainLives(int value){
        livesCount +=value;
        lives.text = livesCount.ToString("0");
    }

    public void AddGems(int value){
        gemCount+=value;
        gems.text = gemCount.ToString("0");
    }

}