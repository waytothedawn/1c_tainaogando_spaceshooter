﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemManager : MonoBehaviour
{
    public GameObject GemPrefab;
    public float timeLauchGem;

    private float currentTime = 0;

    void Awake(){
        StartCoroutine(ThrowGems());
    }


    IEnumerator ThrowGems(){
        while(true){
            Instantiate(GemPrefab,new Vector3(10,Random.Range(-3,3)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchGem);
        }
    }

}
