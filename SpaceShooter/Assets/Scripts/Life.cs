﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{

    [SerializeField] BoxCollider2D collider;
    Vector2 speed;
    [SerializeField] GameObject graphics;
    public AudioSource audioSource;
    private ScoreManager sm;

    void Awake()
    {
        speed.x = Random.Range(-1,-1);
        speed.y = Random.Range(0,0); 
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
    }
        public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Player"){
            StartCoroutine(DestroyLife());

        } else if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }

    }
    IEnumerator DestroyLife(){
           sm.AddScore (50);
        //Desactivo el grafico
        graphics.gameObject.SetActive(false);
        //Elimino el BoxCollider2D
        collider.enabled = false;
        //Lanzo sonido de explosion
        audioSource.Play();
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

}
