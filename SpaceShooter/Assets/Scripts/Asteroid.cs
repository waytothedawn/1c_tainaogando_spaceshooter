﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject graphic;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    [SerializeField] BoxCollider2D collider;
    Vector2 speed;
    private ScoreManager sm;

    
    void Awake()
    {
        speed.x = Random.Range(-6,-1);
        speed.y = Random.Range(-2,2); 
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);

    }
        public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }else if(other.tag == "Laser") {
         sm.AddScore(100);
        StartCoroutine(DestroyAsteroid());
        }

    }
 IEnumerator DestroyAsteroid(){
        //sumar puntos

        //Desactivo el grafico
        graphic.SetActive(false);
 
        //Elimino el BoxCollider2D
        collider.enabled = false;
 
        //Lanzo la partícula
        ps.Play();
 
        //Lanzo sonido de explosion
        audioSource.Play();
 
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
     
        Destroy(this.gameObject);
    }

}
