﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    public GameObject BossPrefab;
    private float timeLauchBoss = 90;

    private float currentTime = 0;

    void Awake(){

    }

    // Update is called once per frame
    
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchBoss){
            currentTime = 0;
            StartCoroutine(ThrowBoss());
        }
    }
    
    IEnumerator ThrowBoss(){
        while(true){
            Instantiate(BossPrefab,new Vector3(10,Random.Range(0,0)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchBoss);
        }
    }

}