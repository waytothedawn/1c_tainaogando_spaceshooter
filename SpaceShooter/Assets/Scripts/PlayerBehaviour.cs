﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{
 
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    private float shootTime;
    public Weapon weapon;
    public Weapon weapon2;
    private float deathtime = 0 ;
    private float death = 3;
    private ScoreManager sm;
    private ScoreManager life;
    private ScoreManager gems;

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audiosource;
    [SerializeField] GameObject shieldgraphic;
    [SerializeField] GameObject loselivegraphic;
    [SerializeField] GameObject gainlivegraphic;
    public int lives;
    private bool ImDead = false;
   
    // Update is called once per frame
    void Update () {
        if(ImDead){

            if (lives <= 0){
            graphics.SetActive(false);
            deathtime++;
            transform.Translate(Vector3.down * Time.deltaTime*5,Space.World);

                if(deathtime>death){
                    SceneManager.LoadScene("GameOverScene");
                }
            }
        }
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
        life = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
        gems = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        shootTime+=Time.deltaTime;
        transform.Translate (axis * speed * Time.deltaTime);
 
        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }
        
        else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
 
        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }
        
        else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
    
    }
    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void SetAxis(float x, float y){
        axis = new Vector2(x,y);
    }

    public void Shoot(){
        if(shootTime>weapon.GetCadencia()){
            shootTime = 0f;
            weapon.Shoot();
            weapon2.Shoot();
        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Asteroid") {
            StartCoroutine(DestroyShip());
        }
        if(other.tag == "LaserEnemy") {
            StartCoroutine(DestroyShip());
        }  
        if(other.tag == "Enemy") {
            StartCoroutine(DestroyShip());
        }
        if(other.tag == "Gem") {
            sm.AddScore(100);
            gems.AddGems(1);
        }
        if(other.tag == "Shield") {
            StartCoroutine(ShieldPowerUp());
        }
        if(other.tag == "Life"){
            StartCoroutine(LifePowerUp());
        }
    }

    IEnumerator DestroyShip(){
        loselivegraphic.SetActive(true);
        //Indico que estoy muerto
        ImDead=true;
        life.QuitLives(1);
        //Me quito una vida
        lives--;
      
        //Desactivo el grafico
        graphics.SetActive(false);
 
        //Elimino el BoxCollider2D
        collider.enabled = false;

        ps.Play();

        audiosource.Play();
        transform.Translate(0,0,0);
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        loselivegraphic.SetActive(false);
        //Miro si tengo mas vidas
        if(lives>0){
            StartCoroutine(inMortal());
        }

        IEnumerator inMortal(){ 
            ImDead = false;
            graphics.SetActive(true);

            for(int i=0; i<15;i++){
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            }
        collider.enabled = true;
        }
    }

IEnumerator LifePowerUp(){
    life.GainLives(1);
    lives++;
    sm.AddScore(300);
    gainlivegraphic.SetActive(true);
    yield return new WaitForSeconds(1.0f);
    gainlivegraphic.SetActive(false);
}

    IEnumerator ShieldPowerUp(){
        shieldgraphic.SetActive(true);
        collider.enabled = false;
    
        yield return new WaitForSeconds(10.0f);

        shieldgraphic.SetActive(false);
        collider.enabled = true;   
    }

}
       
 



	


