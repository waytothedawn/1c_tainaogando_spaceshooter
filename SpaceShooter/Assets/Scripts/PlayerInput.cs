﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour 
{
    public Vector2 axis;
    public PlayerBehaviour ship;


    void Update(){
    axis.x = Input.GetAxis("Horizontal");
    axis.y = Input.GetAxis("Vertical");

    ship.SetAxis(axis);
    
    if(Input.GetButton("Fire1")){
        ship.Shoot();
    }

    }

}

