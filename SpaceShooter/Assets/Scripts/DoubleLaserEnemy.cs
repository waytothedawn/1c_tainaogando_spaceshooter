﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class DoubleLaserEnemy : Weapon
{
    public GameObject laserBullet;
    public float cadencia;
    public float speed;
   
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime, 0, 0);
    }

    public override float GetCadencia()
    {
        return cadencia;
    }
 
    public override void Shoot()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
        GameObject go = Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
        go.transform.Rotate(0,5,0);
        GameObject go2 = Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
        go2.transform.Rotate(0,-5,0);
    }
}